package GameState;

import Entity.GameMap;
import Entity.GameObject;
import Entity.MainPlayer;
import Entity.Player;
import Main.Game;
import Main.Camera;
import Main.KeyInputs;
import Networking.ClientProgram;
import Networking.Network.*; //import all packets
import com.esotericsoftware.kryonet.Client;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import javax.swing.plaf.nimbus.State;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

public class InGame extends BasicGameState {

    public static final int MIN_UPS = 20;
    public static final int NUPS = 20; //network updates
    public static final double MAX_UPDATE_INTERVAL_NS = 1000000000 / MIN_UPS;
    public static final double NETWORK_UPDATE_INTERVAL_NS = 1000000000 / NUPS;
    public static final int DISAPPEARABLE_MESSAGE_DELAY_MS = 2000;

    private static final Color color_sky = new Color(115, 195, 255);
    private static final Color color_border = new Color(255,0,0,50);
    private Image sword_cursor;
    private Image crosshair;

    KeyInputs keyInputs;
    Input gcInput;
    private int state_id;
    private boolean leaveToMenu;
    private boolean toToggleGameInfo;
    private boolean showGameInfo;
    private String leaveMessage;
    private String displayMessage;
    private String disappearableMessage;
    private long disappearableMessageTimer;

    private ClientProgram cp; // for starting the client
    private Client client; // for sending packets

    private int actual_nups;
    private int nups_counter;
    private int actual_ups;
    private int ups_counter;
    private long timer;
    private long currentRenderTime;
    private long currentUpdateTime;
    private long lastUpdateTime;
    private long lastNetworkUpdateTime;
    private boolean doNetworkUpdate;
    private int gameUptime;

    private Camera camera;
    private float main_interpolation; // this is the coefficient of the partial update of a frame
    private float network_interpolation; // this is the coefficient of the partial update of a frame
    private ArrayList<GameObject> gameObjects;
    private ArrayList<GameObject> renderObjects;

    private GameMap map;
    private MainPlayer mainPlayer;
    private Map<Integer, Player> otherPlayers;
    private int score;

    //debugging
    private ConcurrentLinkedQueue<Shape> attacks;


    public static final int SWORD_ATTACK_LENGTH = 85;
    //    public static final int SWORD_ATTACK_LENGTH = 300; //testing
    public static final int SWORD_ATTACK_WIDTH = 20;

    public InGame(int state_id) {
        this.state_id = state_id;
    }

    @Override
    public void init(GameContainer gc, StateBasedGame game) throws SlickException {
        //called once when created in the beginning
        System.out.println("Ingame state added.");
        timer = System.currentTimeMillis();
        gameUptime = 0;
        disappearableMessage = "";
        currentRenderTime = System.nanoTime();
        currentUpdateTime = currentRenderTime;
        lastUpdateTime = currentRenderTime;
        lastNetworkUpdateTime = currentRenderTime;
        doNetworkUpdate = false;
//        gc.setMinimumLogicUpdateInterval((int) (UPDATE_INTERVAL_NS / 1000000)); //min ms for one game update
        gc.setMaximumLogicUpdateInterval((int) (MAX_UPDATE_INTERVAL_NS / 1000000)); //max ms for one game update
        gc.setVSync(true);
        keyInputs = new KeyInputs();
        gcInput = gc.getInput();
        cp = new ClientProgram();
        sword_cursor = new Image("res/cursor/cursor1.png", false, Image.FILTER_NEAREST).getScaledCopy(Game.SCALE);
        crosshair = new Image("res/cursor/cursor_crosshair.png", false, Image.FILTER_NEAREST).getScaledCopy(Game.SCALE);
        gc.setMouseCursor(sword_cursor, Game.MOUSE_HOTSPOT_X * Game.SCALE, Game.MOUSE_HOTSPOT_Y * Game.SCALE);

        map = new GameMap("res/maps/default_map/", 15);
        attacks = new ConcurrentLinkedQueue<>();
    }

    @Override
    public void enter(GameContainer gc, StateBasedGame game) throws SlickException {
        super.enter(gc, game);
        System.out.println("Entered ingame.");
        leaveToMenu = false;
        displayMessage = "";
        otherPlayers = new HashMap<>();
        mainPlayer = new MainPlayer();
        score = 0;
        toToggleGameInfo = false;
        showGameInfo = false;
        camera = new Camera(mainPlayer);
        gameObjects = new ArrayList<>();
        gameObjects.add(mainPlayer);
        renderObjects = new ArrayList<>();
        try {
            Thread.sleep(300); //doing this for initial connection
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        gc.setMouseGrabbed(true);
        //called everytime we enter this state
    }

    @Override
    public void update(GameContainer gc, StateBasedGame game, int delta) throws SlickException {
        currentUpdateTime = System.nanoTime();
        if (leaveToMenu) {
            leaveToMenu(gc, game);
            return;
        }
        if (toToggleGameInfo) {
            toggleGameInfo(gc);
        }
            // TODO: LEAVE FROM GAME BY ESC AND THEN RECONNECT (CHECK IF RECONNECTS SUCCESSFULLY AND DOES NOT KICK AGAIN BECAUSE THIS UPDATE METHOD IS CALLED IN THE BACKGROUND WHILE BEING IN MENU)
        while (currentUpdateTime - lastNetworkUpdateTime >= NETWORK_UPDATE_INTERVAL_NS) {
            lastNetworkUpdateTime += NETWORK_UPDATE_INTERVAL_NS;
            doNetworkUpdate = true;
        }

        if (doNetworkUpdate) {
            doNetworkUpdate(gc, game);
            doNetworkUpdate = false;
            nups_counter++;
        }

        updateDisappearableMessage();

        // Update main player locally
        mainPlayer.update(keyInputs.getKeyPresses(), delta);
        keyInputs.update();
        if (gcInput.isKeyPressed(Input.KEY_P)) {
            gc.setPaused(!gc.isPaused());
        }
        checkPlayerCrossedBoundary();

        //time stuff
        if (System.currentTimeMillis() - timer >= 1000) {
//            System.out.println("x: " + mainPlayer.getX() + "; y: " + mainPlayer.getY());
//            System.out.println(client.isConnected());
            actual_nups = nups_counter;
            nups_counter = 0;
            actual_ups = ups_counter;
            ups_counter = 0;
            timer += 1000;
            gameUptime++;
        }
        ups_counter++;
        lastUpdateTime = System.nanoTime();
    }

    @Override
    public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
        currentRenderTime = System.nanoTime();
        main_interpolation = Math.min(1.0f, (float) ((currentRenderTime - lastUpdateTime) / (1000000000 / Math.max(1, gc.getFPS()))));
        network_interpolation = Math.min(1.0f, (float) ((currentRenderTime - lastNetworkUpdateTime) / NETWORK_UPDATE_INTERVAL_NS));

        //render sky
        g.setColor(color_sky);
        g.fillRect(0, 0, Game.SCALED_WIDTH, Game.SCALED_HEIGHT);

        g.scale(Game.SCALE,Game.SCALE);
        camera.translate(g, main_interpolation);
        //everything that goes beyond and uses screen dimensions must use unscaled dimensions!

        renderObjects.add(map);
        for (GameObject o1 : gameObjects) {
            for (int i = 0; i < renderObjects.size(); i++) {
                GameObject o2 = renderObjects.get(i);
//                System.out.println(o1.getY() + " asdasd " + o2.getY());
                if (o1 instanceof Player && o2 instanceof GameMap) {
                    Player p = (Player) o1;
                    if (p.getCurrentAction() == Player.DEATH_FALL && p.getY() < map.map_middle_point) {
                        renderObjects.add(i, o1);
                        break;
                    }
                }
                if (o1.getY() < o2.getY()) {
                    renderObjects.add(i, o1);
                    break;
                } else if (i == renderObjects.size() - 1) {
                    renderObjects.add(i + 1, o1);
                    break;
                }
            }
        }
        for (GameObject o : renderObjects) {
            float interpolation;
            if (o instanceof MainPlayer) {
                interpolation = main_interpolation;
            } else if (o instanceof Player) {
                interpolation = network_interpolation;
            } else {
                interpolation = 0;
            }
            o.render(g, interpolation);
        }
        renderObjects.clear();


//        //creating the rectangle
//        float[] playerVector = mainPlayer.getPlayerVectorToMouse(gcInput.getMouseX(), gcInput.getMouseY());
//        float x_ratio = playerVector[0];
//        float y_ratio = playerVector[1];
//        // recalc angle so that an actual topdown (and not screen-tilted) angle is returned and sent to the survur
//        float angle = (float) Math.toDegrees(Math.atan(y_ratio/x_ratio)) + 90;
//        if (Math.signum(x_ratio) == Math.signum(-1)) {
//            angle += 180;
//        }
//        float[] points = new float[]{
//                mainPlayer.getX() - 5, mainPlayer.getY()/2,
//                mainPlayer.getX() - SWORD_ATTACK_WIDTH/2, mainPlayer.getY()/2 - SWORD_ATTACK_LENGTH,
//                mainPlayer.getX() + SWORD_ATTACK_WIDTH/2, mainPlayer.getY()/2 - SWORD_ATTACK_LENGTH,
//                mainPlayer.getX() + 5, mainPlayer.getY()/2
//        };
//        Shape attackHitbox = new Polygon(points);
//        //getting the degrees from player's attack and rotating the sword hitbox as needed.
//        attackHitbox = attackHitbox.transform(Transform.createRotateTransform((float) Math.toRadians(angle), mainPlayer.getX(), mainPlayer.getY()/2));
//        g.setColor(Color.red);
//        g.draw(attackHitbox);
//        g.setColor(color_border);
//        g.fill(attackHitbox);
//

//        int cw = 7;
//        int ch = 18;
//        g.setColor(Color.red);
//        Shape playerHitbox = new Ellipse(mainPlayer.getX(), mainPlayer.getY()/2 - ch, cw, ch);
//        g.draw(playerHitbox);

//        int circleRadius = 190/2;
//        g.setColor(color_border);
//        g.fillOval(mainPlayer.getX() - circleRadius, mainPlayer.getY()/2 - circleRadius/2, circleRadius*2, circleRadius);

        //debugging
//        for (Shape shape : attacks) {
//            g.setColor(Color.red);
//            g.draw(shape);
//            g.setColor(color_border);
//            g.fill(shape);
//        }
//        g.setColor(Color.red);
//        g.fillOval((mainPlayer.getLastX() + (mainPlayer.getX() - mainPlayer.getLastX()) * main_interpolation) -10f,
//                (mainPlayer.getLastY() + (mainPlayer.getY() - mainPlayer.getLastY()) * main_interpolation)/2 - 26f,
//                20f,
//                20f);

        //render hud
        camera.translateBack(g);
        g.scale(1f/Game.SCALE, 1f/Game.SCALE);
        // everything that goes beyond is unscaled and untranslated
        g.setColor(Color.white);
        if (showGameInfo) {
            g.drawString("UPS: " + Integer.toString(actual_ups), 10, 30);
            g.drawString("NUPS: " + Integer.toString(actual_nups), 10, 50);
        }
        g.drawString(displayMessage, Game.SCALED_WIDTH/2 - (int) (displayMessage.length() * 4.5), Game.SCALED_HEIGHT/2 - 150 * Game.SCALE);
        g.drawString(disappearableMessage, Game.SCALED_WIDTH/2 - (int) (disappearableMessage.length() * 4.5), Game.SCALED_HEIGHT/2 + 50 * Game.SCALE);
        //render crosshair if needed
        if (mainPlayer.getCurrentAction() == Player.PREPARE_GUN) {
//            System.out.println(mainPlayer.getCrosshairX());
            g.drawImage(
                    crosshair,
                    mainPlayer.getCrosshairX() - crosshair.getWidth()/2 + (Game.MOUSE_HOTSPOT_X) * Game.SCALE,
                    mainPlayer.getCrosshairY() - crosshair.getHeight()/2 + (Game.MOUSE_HOTSPOT_Y) * Game.SCALE
//                    mainPlayer.getCrosshairX() - crosshair.getWidth() / 2,
//                    (mainPlayer.getCrosshairY() - crosshair.getHeight() / 2) / 2
            );} //1 and 3 are mouse cursor hotspot offsets

    }

    public void doNetworkUpdate(GameContainer gc, StateBasedGame game) {

        for (Player p : otherPlayers.values()) {
            p.updateLastPos(); //update last coordinate values
        }

        // Update received data from server
        ConcurrentLinkedQueue<Object> receivedPackets = cp.getReceivedPackets();
        Object o;
        while ((o = receivedPackets.poll()) != null) {
            if (o instanceof PacketAddPlayer) {
                PacketAddPlayer packet = (PacketAddPlayer) o;
                System.out.println("a player has joined.");
                Player p = new Player();
                p.setPosition(packet.x, packet.y, 0);
                otherPlayers.put(packet.id, p);
                gameObjects.add(p);

            } else if (o instanceof PacketRemovePlayer) {
                PacketRemovePlayer packet = (PacketRemovePlayer) o;
                System.out.println("a player has left.");
                gameObjects.remove(otherPlayers.remove(packet.id));

            } else if (o instanceof PacketUpdateX) {
                PacketUpdateX packet = (PacketUpdateX) o;
//                System.out.println("player moved x.");
                otherPlayers.get(packet.id).setX(packet.x);

            } else if (o instanceof PacketUpdateY) {
                PacketUpdateY packet = (PacketUpdateY) o;
//                System.out.println("player moved y.");
                otherPlayers.get(packet.id).setY(packet.y);

            } else if (o instanceof PacketUpdateZ) {
                PacketUpdateZ packet = (PacketUpdateZ) o;
//                System.out.println("player moved y.");
                otherPlayers.get(packet.id).setZ(packet.z);

            } else if (o instanceof PacketUpdateDirection) {
                PacketUpdateDirection packet = (PacketUpdateDirection) o;
                otherPlayers.get(packet.id).setDirection(packet.direction);

            } else if (o instanceof PacketUpdateAction) {
                PacketUpdateAction packet = (PacketUpdateAction) o;
                otherPlayers.get(packet.id).setCurrentAction(packet.action, packet.restartAnim);

            } else if (o instanceof PacketSetPlayerPosition) {
                PacketSetPlayerPosition packet = (PacketSetPlayerPosition) o;
                mainPlayer.setPosition(packet.x, packet.y, 0);

            } else if (o instanceof PacketKillPlayer) {
//                    PacketKillPlayer packet = (PacketKillPlayer) o;
                if (mainPlayer.isAlive()) camera.shake(2, 4);
                mainPlayer.die(-1);

            } else if (o instanceof PacketRespawnPlayer) {
//                System.out.println("received respawn packet");
                PacketRespawnPlayer packet = (PacketRespawnPlayer) o;
                mainPlayer.setPosition(packet.x, packet.y, 0);
                mainPlayer.setAmmo(packet.ammo);
                mainPlayer.rise();

            } else if (o instanceof PacketSwordClash) {
                PacketSwordClash packet = (PacketSwordClash) o;
                camera.shake(3, 6);
                mainPlayer.swordClash(packet.x_ratio, packet.y_ratio);

            } else if (o instanceof PacketAttackShoot) {
                PacketAttackShoot packet = (PacketAttackShoot) o;
                otherPlayers.get(packet.id).executeShot(packet.shot);

            } else if (o instanceof PacketUpdateScore) {
                camera.shake(1, 4);
                PacketUpdateScore packet = (PacketUpdateScore) o;
                score = packet.score;
                setDisappearableMessage("Score: " + packet.score);

            } else if (o instanceof PacketKick) {
                PacketKick packet = (PacketKick) o;
                leaveToMenu = true;
                leaveMessage = "Kicked by server, reason: " + packet.message;

            } else if (o instanceof PacketReady) {
                mainPlayer.toggleReady();
                if (mainPlayer.isReady()) {
                    displayMessage = "Lobby: Ready";
                } else {
                    displayMessage = "Lobby: Press R to get ready";
                }

            } else if (o instanceof PacketMessage) {
                PacketMessage packet = (PacketMessage) o;
                displayMessage = packet.message;
                System.out.println("messageReceived: " + packet.message);

            }
            //debugging
//            else if (o instanceof DebugAttack) {
//                DebugAttack packet = (DebugAttack) o;
//                if (attacks.size() > 3) attacks.poll();
//                attacks.add(packet.shape);
//            }
        }

        //Update main player on server
        PacketUpdateX packet_x = new PacketUpdateX();
        packet_x.x = mainPlayer.getX();
        client.sendTCP(packet_x);

        PacketUpdateY packet_y = new PacketUpdateY();
        packet_y.y = mainPlayer.getY();
        client.sendTCP(packet_y);

        PacketUpdateZ packet_z = new PacketUpdateZ();
        packet_z.z = mainPlayer.getZ();
        client.sendTCP(packet_z);

        PacketUpdateDirection packet_dir = new PacketUpdateDirection();
        packet_dir.direction = mainPlayer.getDirection();
        client.sendTCP(packet_dir);

        if (mainPlayer.wasActionUpdated()) {
            PacketUpdateAction packet_act = new PacketUpdateAction();
            packet_act.action = mainPlayer.getCurrentAction();
            packet_act.restartAnim = mainPlayer.wasAnimRestarted();
            client.sendTCP(packet_act);
            mainPlayer.packetUpdateActionSent();
        }
        if (mainPlayer.wasShotExecuted()) {
            PacketAttackShoot packet_shot = new PacketAttackShoot();
            packet_shot.shot = mainPlayer.getShot();
            client.sendTCP(packet_shot);
            mainPlayer.packetExecuteShotSent();
            camera.shake(2, 5);
        }
        if (mainPlayer.wasAliveUpdated()) {
            PacketUpdateAlive packet_alive = new PacketUpdateAlive();
            packet_alive.alive = mainPlayer.isAlive();
            client.sendTCP(packet_alive);
            mainPlayer.packetAliveUpdateSent();
        }
    }

//    public void renderBorder(Graphics g, int size, int height) {
//        g.setColor(color_border);
//        for (int i = 0; i < size; i++) {
//            //upper part
//            g.drawLine(0, -height - i, tileWidth/2 * mapSizeX, tileHeight/2 * mapSizeY - height - i);
//            g.drawLine(0, -height - i, -tileWidth/2 * mapSizeX, tileHeight/2 * mapSizeY - height - i);
//            //lower part
//            g.drawLine(0, tileHeight * mapSizeY - height - i, tileWidth/2 * mapSizeX, tileHeight/2 * mapSizeY - height - i);
//            g.drawLine(0, tileHeight * mapSizeY - height - i, -tileWidth/2 * mapSizeX, tileHeight/2 * mapSizeY - height - i);
//        }
//    }

    public void setDisappearableMessage(String message) {
        disappearableMessage = message;
        disappearableMessageTimer = System.currentTimeMillis();
    }

    public void updateDisappearableMessage() {
        if (disappearableMessage.equals("")) return;
        if (System.currentTimeMillis() - disappearableMessageTimer >= DISAPPEARABLE_MESSAGE_DELAY_MS) {
            disappearableMessage = "";
        }
    }

    public void checkPlayerCrossedBoundary() { // server also confirms this
        float checkY;
        if (mainPlayer.getY() > map.map_middle_point) {
            checkY = map.map_actual_height - mainPlayer.getY();
        } else {
            checkY = mainPlayer.getY();
        }
        if (mainPlayer.getX() < -checkY || mainPlayer.getX() > checkY) {
//            System.out.println("player is out of bounds");
            mainPlayer.die(Player.DEATH_BY_FALLING_OFF);
        }
    }

    @Override
    public void keyPressed(int key, char c) {

        if (key == Input.KEY_ESCAPE) {
            leaveToMenu = true;
            leaveMessage = "Left the game myself";
        } else if (key == Input.KEY_I) {
            toToggleGameInfo = true;
        } else if (mainPlayer.isAlive()) {
            if (key == Input.KEY_R) {
                PacketReady packet_ready = new PacketReady();
                client.sendTCP(packet_ready);
            } else if (!mainPlayer.isBusy()) {
                if (key == Input.KEY_SPACE) {
                    //tumble turned off
//                mainPlayer.tumble(gcInput.getMouseX(), gcInput.getMouseY());
                }

            }
        }
        keyInputs.keyPressed(key);
    }

    @Override
    public void keyReleased(int key, char c) {
        keyInputs.keyReleased(key);
    }

    @Override
    public void mousePressed(int button, int x, int y) { //handle mouse inputs instantly
        super.mousePressed(button, x, y);
//        System.out.println(button + " " + x + " " + y);
        if (!mainPlayer.isBusy() && mainPlayer.isAlive()) {
            if (button == Input.MOUSE_LEFT_BUTTON) {
//                System.out.println("location: " + x + ", " + y);
                PacketAttackSlash packet_slash = new PacketAttackSlash();
                packet_slash.angle = mainPlayer.slashAttack(x, y);
                client.sendTCP(packet_slash);
                return;
            }
            if (button == Input.MOUSE_RIGHT_BUTTON) {
                mainPlayer.shoot(x, y);
            }
        }
    }

    public void toggleGameInfo(GameContainer gc) {
        showGameInfo = !showGameInfo;
        gc.setShowFPS(!gc.isShowingFPS());
        toToggleGameInfo = false;
    }

    public void connectToServer(String ip, int port) throws Exception { //throw further into the Menu
        cp.connect(ip, port);
        client = cp.getClient();
    }

    private void leaveToMenu(GameContainer gc, StateBasedGame game) {
        client.stop();
        ( (Menu) game.getState(Game.STATE_MENU)).setNotificationMessageText(leaveMessage);
        gc.getInput().clearKeyPressedRecord();
        game.enterState(Game.STATE_MENU);

    }
    @Override
    public int getID() {
        return state_id;
    }
}
