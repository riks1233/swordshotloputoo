package Main;

import Entity.MainPlayer;
import Entity.Player;
import org.newdawn.slick.Graphics;

public class Camera {
    private float x;
    private float y;
    private float last_x;
    private float last_y;
    private int shakeDuration;
    private int shakeMagnitude;
    private float playerLocalX;
    private float playerLocalY;
    private MainPlayer target;
    private float factor;

    public Camera(MainPlayer target) {
        this.target = target;
        x = 0;
        y = 0;
        playerLocalX = 0;
        playerLocalY = 0;
        last_x = 0;
        last_y = 0;
        factor = 0.2f;
    }

    public void translate(Graphics g, float interpolation) {
        // use unscaled screen dimensions!
        //divide WHOLE height translation by 2 (and not only screen height) so that Y is rendered properly (as we have tilted view)
        float target_x = (target.getLastX() + (target.getX() - target.getLastX()) * interpolation);
        float target_y = (target.getLastY() + (target.getY() - target.getLastY()) * interpolation);
        if (target.getCurrentAction() == Player.DEATH_FALL) { //when falling, stop camera movement
            target_x = target.getDied_at_x();
            target_y = target.getDied_at_y();
        }
//        x = -target_x + Game.WIDTH / 2;
//        y = (-target_y + Game.HEIGHT) / 2;
//        System.out.println(interpolation);
        float help_x = (-target_x + Game.WIDTH / 2 - x) + Game.SCALED_WIDTH / 2;
        float help_y = ((-target_y + Game.HEIGHT) / 2 - y) + Game.SCALED_HEIGHT / 2;
        target.setPlayer_local_x(help_x - (help_x - Game.SCALED_WIDTH / 2) * 2);
        target.setPlayer_local_y(help_y - (help_y - Game.SCALED_HEIGHT / 2) * 2);
        target.setCamMovedByX(x - last_x);
        target.setCamMovedByY(y - last_y);
//        target.setCamMovedByX((-target_x + Game.WIDTH / 2 - x) * factor);
//        target.setCamMovedByY(((-target_y + Game.HEIGHT) / 2 - y) * factor);
        //TODO: fix and use this | EDIT fixed by limiting update rate to framerate
        last_x = x;
        last_y = y;
        x += (-target_x + Game.WIDTH / 2 - x) * factor;
        y += ((-target_y + Game.HEIGHT) / 2 - y) * factor;


        if (shakeDuration > 0) {
            x += Game.randomWithRange(-shakeMagnitude, shakeMagnitude);
            y += Game.randomWithRange(-shakeMagnitude, shakeMagnitude);
            shakeDuration--;
        }
        g.translate(x, y);
    }

    public void translateBack(Graphics g) {
        g.translate(-x, -y);
    }

    public void shake(int duration, int magnitude) { //duration in frames (maybe better to remake to use time as duration)
        shakeDuration = duration;
        shakeMagnitude = magnitude;
    }

    public void setTarget(MainPlayer target) {
        this.target = target;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
