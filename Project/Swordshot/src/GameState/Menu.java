package GameState;

import Main.Game;
import org.lwjgl.Sys;
import org.newdawn.slick.*;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.net.UnknownHostException;

public class Menu extends BasicGameState {

    public static final int ipEntrySize = 200;
    public static final int portEntrySize = 80;
    public static final int gapSize = 20;

    private Input in;
    private int state_id;

    private String ip;
    private int port;

    private TrueTypeFont entryFont;
    private TrueTypeFont notificationMessageFont;
    private TextField ipEntry;
    private TextField portEntry;

    private String notificationMessageText;

    public Menu(int state_id) {
        this.state_id = state_id;
//        port = 80;
    }

    @Override
    public void init(GameContainer gc, StateBasedGame game) throws SlickException {
        System.out.println("Menu state added.");
        gc.setVSync(true);
        in = gc.getInput();
        java.awt.Font awtFont = new java.awt.Font("Arial", java.awt.Font.PLAIN, 20);
        entryFont = new TrueTypeFont(awtFont, true);
        awtFont = new java.awt.Font("Arial", java.awt.Font.PLAIN, 12);
        notificationMessageFont = new TrueTypeFont(awtFont, true);
        notificationMessageText = "";

    }

    @Override
    public void enter(GameContainer gc, StateBasedGame game) throws SlickException { //called when state is entered
        super.enter(gc, game);
        System.out.println("Entered menu.");
        gc.setShowFPS(false);
        ipEntry = new TextField(gc, entryFont, Game.SCALED_WIDTH / 2 - (ipEntrySize + portEntrySize + gapSize) / 2 , Game.SCALED_HEIGHT / 2, ipEntrySize, 30);
        ipEntry.setBackgroundColor(Color.white);
        ipEntry.setTextColor(Color.black);
        ipEntry.setText("");
        portEntry = new TextField(gc, entryFont, Game.SCALED_WIDTH / 2 - (ipEntrySize + portEntrySize + gapSize) / 2 + ipEntrySize + gapSize, Game.SCALED_HEIGHT / 2, portEntrySize, 30);
        portEntry.setBackgroundColor(Color.white);
        portEntry.setTextColor(Color.black);
        portEntry.setText("");
    }

    @Override
    public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
        g.drawString("IP address:", Game.SCALED_WIDTH / 2 - (ipEntrySize + portEntrySize + gapSize) / 2, Game.SCALED_HEIGHT / 2 - 30);
        g.drawString("port:", Game.SCALED_WIDTH / 2 - (ipEntrySize + portEntrySize + gapSize) / 2 + ipEntrySize + gapSize, Game.SCALED_HEIGHT / 2 - 30);
        ipEntry.render(gc, g);
        portEntry.render(gc, g);
        g.setFont(notificationMessageFont);
        g.drawString(notificationMessageText, 20, Game.SCALED_HEIGHT / 2 + 40);
    }

    @Override
    public void update(GameContainer gc, StateBasedGame game, int delta) throws SlickException {
//        Input in = gc.getInput();
        if (in.isKeyPressed(Input.KEY_ENTER)) { // || in.isKeyPressed(Input.KEY_SPACE)
            ipEntry.deactivate(); //text field is no longed focused, but still persists and can be clicked
            portEntry.deactivate(); //text field is no longed focused, but still persists and can be clicked
            String ipEntryText = ipEntry.getText();
            String portEntryText = portEntry.getText();
            if (ipEntryText != "") {
                ip = ipEntryText;
            } else {
                ip = "localhost";
            }
            try {
                if (portEntryText != "") {
                    port = Integer.parseInt(portEntryText);
                } else {
                    port = 27960; //default port
                }
                ( (InGame) game.getState(Game.STATE_INGAME)).connectToServer(ip, port); //we are getting the ingame state certainly, so we cast to it
                game.enterState(Game.STATE_INGAME);
                leave();
            } catch (NumberFormatException e) {
                notificationMessageText = "Error: port entry has invalid characters (must consist of number characters only)";
            } catch (UnknownHostException e) {
                notificationMessageText = "Unknown host: " + ipEntryText;
            } catch (Exception e) {
                e.printStackTrace();
                notificationMessageText = "Error message: " + e.getMessage();
            }
        } else if (in.isKeyPressed(Input.KEY_ESCAPE)) {
            System.exit(0);
        }
    }
    // call when leaving this state to disable textfields as they somehow percieve in the background and clicking on the place where they were causes all user input to be focused on them even in other game states :(
    public void leave() {
        ipEntry.setLocation(-ipEntry.getWidth(), -ipEntry.getHeight());
        portEntry.setLocation(-portEntry.getWidth(), -portEntry.getHeight());
    }

    public void setNotificationMessageText(String message) {
        notificationMessageText = message;
    }

    @Override
    public int getID() {
        return state_id;
    }


}