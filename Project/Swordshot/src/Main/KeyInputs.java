package Main;

import org.newdawn.slick.Input;

import java.util.HashMap;
import java.util.LinkedHashSet;

public class KeyInputs {

    private HashMap<Integer, Boolean> wasKeyReleasedMap;
    private LinkedHashSet<Integer> keyPresses;

    public KeyInputs() {
        wasKeyReleasedMap = new HashMap<>();
        wasKeyReleasedMap.put(Input.KEY_W, true);
        wasKeyReleasedMap.put(Input.KEY_A, true);
        wasKeyReleasedMap.put(Input.KEY_S, true);
        wasKeyReleasedMap.put(Input.KEY_D, true);

        keyPresses = new LinkedHashSet<>();
    }


    public LinkedHashSet<Integer> getKeyPresses() {
        return keyPresses;
    }


    public void update() { // update keyinputs and clear those that have been released
        wasKeyReleasedMap.forEach((keyCode, wasReleased) -> {
            if (wasReleased) {
                keyPresses.remove(keyCode);
            };
        });
    }

    public void keyPressed(Integer keyCode) {
        if (wasKeyReleasedMap.containsKey(keyCode) ){ //&& wasKeyReleasedMap.get(keyCode) == true
            wasKeyReleasedMap.put(keyCode, false);
            // if the keycode is already present in the list then remove it and add it again to the end of the list
            keyPresses.remove(keyCode); //returns boolean, is safe
            keyPresses.add(keyCode);
        }
    }

    public void keyReleased(Integer keyCode) {
        if (wasKeyReleasedMap.containsKey(keyCode)) {
            wasKeyReleasedMap.put(keyCode, true);
        }
    }
}
