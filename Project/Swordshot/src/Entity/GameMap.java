package Entity;

import Main.Game;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GameMap implements GameObject {

    private int[] mapData;
    private Image[] mapImages;
    private Image[] mapOverlays;
    private Image[] overlayImages; // dark or light overlay for a map sector to be darker or lighter compared to others.
    private int mapSizeX;
    private int mapSizeY;
    private int tileWidth;
    private int tileHeight;
    public int map_actual_height;
    public int map_middle_point;

    public GameMap(String ref, int ignore_height) {
        try {
            overlayImages = new Image[] {new Image("res/maps/dark_overlay.png", false, Image.FILTER_NEAREST), new Image("res/maps/light_overlay.png", false, Image.FILTER_NEAREST)};
        } catch (SlickException e) {
            e.printStackTrace();
        }
        createMap("res/maps/default_map/", 15);
    }

    @Override
    public float getY() {
        return 0;
    }

    public void render(Graphics g, float interpolation) {
        renderMap(g);
    }

    public void renderMap(Graphics g) {
        int row;
        int shift_x;
        int shift_y;
        int draw_x;
        int draw_y;
        for (int i = 0; i < mapData.length; i++) {
            row = i / mapSizeX;
            shift_x = -(tileWidth / 2) * row - tileWidth/2;
            shift_y = (tileHeight / 2) * row;
            draw_x = (tileWidth / 2 * (i % mapSizeX)) + shift_x;
            draw_y = (tileHeight / 2  * (i % mapSizeX)) + shift_y;
            g.drawImage(mapImages[mapData[i]], draw_x, draw_y);
            if (!(mapData[i] == 0)) {
                g.drawImage(mapOverlays[i], draw_x, draw_y);
            }
        }
    }

    public void createMap(String map_directory, int ignore_height) {
        try {
            String line = null;
            BufferedReader br = new BufferedReader(new FileReader(map_directory + "map.txt")); //map_directory + "map.txt"

            mapSizeX = Integer.parseInt(br.readLine());
            mapSizeY = Integer.parseInt(br.readLine());
            tileWidth = Integer.parseInt(br.readLine());
//            tileHeight = Integer.parseInt(br.readLine()) - ignore_height; // height that is ignored because it is already a side of a tile
            tileHeight = tileWidth / 2;
            map_actual_height = mapSizeX * tileWidth;
            map_middle_point = map_actual_height / 2;
            int texture_count = Integer.parseInt(br.readLine());
            int[] map_data = new int[mapSizeX * mapSizeY];
            Image[] map_overlays = new Image[mapSizeX * mapSizeY];
            int current_y = 0;
            while ((line = br.readLine()) != null) {
                String[] map_data_line = line.split(" ");
                for (int i = 0; i < mapSizeX; i++) {
                    map_data[current_y * mapSizeX + i] = Integer.parseInt(map_data_line[i]);
                    Image chunk_overlay = new Image(overlayImages[Game.randomWithRange(0, 1)].getTexture());
                    chunk_overlay.setAlpha(Game.randomWithRange(0, 2) / 100f);
                    map_overlays[current_y * mapSizeX + i] = chunk_overlay;
                }
                current_y += 1;
            }
            br.close();
            mapData = map_data;
            mapOverlays = map_overlays;

            Image[] map_images = new Image[texture_count + 1];
            map_images[0] = new Image(0, 0); // create a 0 image so that 0 in map represents emptyness
            for (int i = 1; i < map_images.length; i++) {
                map_images[i] = new Image(map_directory + Integer.toString(i) + ".png", false, Image.FILTER_NEAREST);
            }
            mapImages = map_images;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
