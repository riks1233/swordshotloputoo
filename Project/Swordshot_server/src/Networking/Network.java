package Networking;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;
import org.newdawn.slick.geom.Shape;

// This class is a convenient place to keep things common to both the client and server.
public class Network {

    // This registers objects that are going to be sent over the network.
    static public void register (EndPoint endPoint) {
        Kryo kryo = endPoint.getKryo();
        kryo.register(PacketUpdateX.class);
        kryo.register(PacketUpdateY.class);
        kryo.register(PacketUpdateZ.class);
        kryo.register(PacketAddPlayer.class);
        kryo.register(PacketRemovePlayer.class);
        kryo.register(PacketSetPlayerPosition.class);
        kryo.register(PacketUpdateDirection.class);
        kryo.register(PacketUpdateAction.class);
        kryo.register(PacketAttackSlash.class);
        kryo.register(PacketSwordClash.class);
        kryo.register(PacketAttackShoot.class);
        kryo.register(PacketKillPlayer.class);
        kryo.register(PacketRespawnPlayer.class);
        kryo.register(PacketUpdateAlive.class);
        kryo.register(PacketKick.class);
        kryo.register(PacketReady.class);
        kryo.register(PacketMessage.class);
        kryo.register(PacketUpdateScore.class);
        kryo.register(float[].class);

        // debugging
//        kryo.register(DebugAttack.class);
//        kryo.register(org.newdawn.slick.geom.Polygon.class);
    }

    static public class PacketAddPlayer {
        public int id;
        public float x;
        public float y;
    }

    static public class PacketRemovePlayer {
        public int id;
    }

    static public class PacketUpdateX {
        public int id;
        public float x;
    }

    static public class PacketUpdateY {
        public int id;
        public float y;
    }

    static public class PacketUpdateZ {
        public int id;
        public float z;
    }

    static public class PacketSetPlayerPosition {
        public float x;
        public float y;
    }

    static public class PacketUpdateDirection {
        public int id;
        public int direction;
    }

    static public class PacketUpdateAction {
        public int id;
        public int action;
        public boolean restartAnim;
    }

    static public class PacketAttackSlash {
//        public int id;
        public float angle;
    }

    static public class PacketSwordClash {
        public float x_ratio;
        public float y_ratio;
    }

    static public class PacketAttackShoot {
        public int id;
        public float[] shot;
    }

    static public class PacketKillPlayer {
    }

    static public class PacketRespawnPlayer {
        public float x;
        public float y;
        public int ammo;
    }

    static public class PacketUpdateAlive {
        public boolean alive;
    }

    static public class PacketKick {
        public String message;
    }

    static public class PacketReady {
    }

    static public class PacketMessage {
        public String message;
    }

    static public class PacketUpdateScore {
        public int score;
    }




    // debugging
//    static public class DebugAttack {
//        public Shape shape;
//    }

}