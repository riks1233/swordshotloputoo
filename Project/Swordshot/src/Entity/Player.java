package Entity;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import java.awt.image.BufferedImage;

/*
Direction wheel:
            0
        5   |   1
         \     /
     6 -         - 2
         /     \
        7   |   3
            4

These are just arbitrary numbers. For example, if the player is looking left, direction is set to 6. This is updated in the updateDirection() method.
if the direction is > 4, then we have to take (direction - 4) animation and flip it horizontally. For example, if the player is facing left-down (7) then we have to take the sprite (animation) under number 3 and flip it horizontally.
*/

public class Player implements GameObject{

    public static final int IDLE = 0;
    public static final int RUN = 1;
    public static final int ATTACK_SLASH = 2;
    public static final int PREPARE_GUN = 3;
    public static final int SWORD_CLASH = 4;
    public static final int ATTACK_SHOOT = 5;
    public static final int TUMBLE = 6;
    public static final int DEATH_KILLED = 7;
    public static final int DEATH_FALL = 8;

    public static final int DEATH_BY_FALLING_OFF = 0;

    public static final int shot_trace_distance = 2000;
    public static final int shot_trace_start_radius = 50;

    public static final float sword_slash_dash_force = 0.5f;
    public static final float sword_clash_force = 1.5f;
    public static final float recoil_force = 0.2f;
    public static final float tumble_force = 1f;
    //busy timers in ms
    public static final int bt_sword_slash = 350;
    public static final int bt_sword_clash = 700;
    public static final int bt_prepare_gun = 400;
    public static final int bt_shoot = 300;
    public static final int bt_tumble = 190;
    public static final int tumble_cooldown = 2000;

    public static final Color shadow_color = new Color(0,0,0, 50);

    DirectedAnimation[] animations;
    DirectedAnimation currentAnim;
    int currentAction;

    int direction;

    float x;
    float y; // y must be divided by 2 when rendering
    float z; // z is negative upwards and is also divided by 2 when rendering (goes with y render)
    //previous tick coordinates (needed for partial ticks)
    float lastX;
    float lastY;
    float lastZ;

    float player_height;
    float player_width;

    boolean doRenderShot;
    float[] shot;


    public Player() {

        animations = new DirectedAnimation[9];
        animations[IDLE] = new DirectedAnimation("res/sprites/player/stickman/idle_ss.png", false, new int[]{1}, 40, 40, 20, 37);
        animations[RUN] = new DirectedAnimation("res/sprites/player/stickman/run_ss.png", true, new int[]{2,4,2,1,2,2,4,2,1,2}, 40, 40, 20, 33);
        animations[ATTACK_SLASH] = new DirectedAnimation("res/sprites/player/stickman/slash_ss.png", false, new int[]{1, 2, 2, 1, 1}, 120, 120, 60, 70);
        animations[SWORD_CLASH] = new DirectedAnimation("res/sprites/player/stickman/clash_ss.png", true, new int[]{3,3}, 40, 40, 20, 26);
        animations[PREPARE_GUN] = new DirectedAnimation("res/sprites/player/stickman/prepare_ss.png", false, new int[]{3,1,3,1}, 46, 46, 23, 37);
        animations[ATTACK_SHOOT] = new DirectedAnimation("res/sprites/player/stickman/shoot_ss.png", false, new int[]{1, 1}, 46, 46, 23, 37);
//        animations[TUMBLE] = new DirectedAnimation("res/sprites/player/stickman/run_ss.png", false, new int[]{3,3}, 40, 40, 20, 33);
        animations[TUMBLE] = new DirectedAnimation("res/sprites/player/stickman/idle_ss.png", false, new int[]{1}, 40, 40, 20, 37);
        animations[DEATH_KILLED] = new DirectedAnimation("res/sprites/player/stickman/death_ss.png", false, new int[]{2, 1, 1, 2, 4, 10, 5, 3, 2, 1}, 80, 80, 40, 56);
        animations[DEATH_FALL] = new DirectedAnimation("res/sprites/player/stickman/fall_ss.png", false, new int[]{1}, 40, 40, 20, 36);

        currentAction = IDLE;
        currentAnim = animations[currentAction];

        x = 0;
        y = 0;
        z = 0;
        //previous tick coordinates (needed for partial ticks)
        lastX = 0;
        lastY = 0;
        lastZ = 0;

        direction = 4;

        player_height = 18;
        player_width = 15;

        doRenderShot = false;
        shot = new float[4];
    }

    public void setPosition(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        lastX = x;
        lastY = y;
        lastZ = z;
    }

    public void updateLastPos() {
        lastX = x;
        lastY = y;
        lastZ = z;
    }

    public void render(Graphics g, float interpolation) {
        //for shadow
        float calc_x = lastX + (x - lastX) * interpolation;
        float calc_yz = (lastY + lastZ + (y - lastY + z - lastZ) * interpolation) / 2f; //2f for tilted rendering Y
        if (!(currentAction == DEATH_FALL)) { //if the guy is falling (out of map) dont draw the shadow
            g.setColor(shadow_color);
            g.fillOval(calc_x - player_width / 2, calc_yz - player_width / 4 , player_width, player_width / 2);
        }
        //for animation
        calc_x -= currentAnim.getAnchor_x();
        calc_yz -= currentAnim.getAnchor_y();

        int dir = getDirection();
        if (dir > 4) { //flip the image
            dir -= 4;
            g.drawImage(getCurrentImage(dir).getFlippedCopy(true,false), calc_x, calc_yz);
        } else {
            g.drawImage(getCurrentImage(dir), calc_x, calc_yz);
        }
        currentAnim.render();
        if (doRenderShot) {
            g.setColor(Color.yellow);
            g.drawLine(
                    shot[0] + shot_trace_start_radius * shot[2],
                    (shot[1] + shot_trace_start_radius * shot[3]) / 2 - player_height,
                    shot[0] + shot[2] * shot_trace_distance,
                    (shot[1] + shot[3] * shot_trace_distance) / 2);
            doRenderShot = false;
        }
    }

    public void executeShot(float[] shot) {
        this.shot = shot;
        doRenderShot = true;
    }

    public Image getCurrentImage(int direction) {
        return currentAnim.getImage(direction);
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getCurrentAction() {
        return currentAction;
    }

    public void setCurrentAction(int currentAction, boolean restartAnim) {
        this.currentAction = currentAction;
        updateAnimation();
        if (restartAnim) currentAnim.restart();
    }

    public void updateAnimation() {
        currentAnim = animations[currentAction];
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getLastX() {
        return lastX;
    }

    public float getLastY() {
        return lastY;
    }

    public float getLastZ() {
        return lastZ;
    }
}
