package Networking.Server;
import com.esotericsoftware.kryonet.Connection;

public class ServerReceivedPacket {
    public Connection c;
    public Object o;

    public ServerReceivedPacket(Connection c, Object o) {
        this.c = c;
        this.o = o;
    }
}
