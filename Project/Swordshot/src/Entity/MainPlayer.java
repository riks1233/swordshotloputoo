package Entity;

import Main.Game;
import org.lwjgl.Sys;
import org.newdawn.slick.Input;

import java.util.LinkedHashSet;

public class MainPlayer extends Player {

    public static final float DEFAULT_DRAG_FORCE = 0.06f;

    private float moveSpeed;
    private float accelerationMultiplier;
    private float diagonalMultiplier;
    private float dragForce;

    private float died_at_x;
    private float died_at_y;


//    float netX;
//    float netY;
//    float netZ;

    float speedX;
    float speedY;
    float speedZ;

    float off_x;
    float off_y;
    boolean left;
    boolean right;
    boolean up;
    boolean down;
    boolean diagonal;

    float player_local_x;
    float player_local_y;
    float crosshair_x;
    float crosshair_y;
    float camMovedByX;
    float camMovedByY;

    boolean busy;
    boolean alive;
    boolean ready; //ready to start the game
    int currentBusyTimer;
    long startedBeingBusyAt;
    long previousTumbleWasAt;

    int ammo;

    boolean packetAliveUpdated;
    boolean packetActionUpdated;
    boolean packetRestartAnim;
    boolean packetExecuteShot;

    public MainPlayer() {
        super();
        moveSpeed = 0.22f;
//        moveSpeed = 0.1f;
        accelerationMultiplier = 0.3f;
        diagonalMultiplier = 0.707f;
        dragForce = DEFAULT_DRAG_FORCE;

        speedX = 0;
        speedY = 0;
        speedZ = 0;

        off_x = 0;
        off_y = 0;
        left = false;
        right = false;
        up = false;
        down = true;
        diagonal = false;

        player_local_x = 0; //these two are set by camera
        player_local_y = 0;
        crosshair_x = 0;
        crosshair_y = 0;
        camMovedByX = 0;
        camMovedByY = 0;
        died_at_x = 0;
        died_at_y = 0;

        busy = false;
        alive = false;
        ready = false;
        currentBusyTimer = bt_sword_slash;
        startedBeingBusyAt = 0;
        previousTumbleWasAt = 0;

        ammo = 0;

        packetAliveUpdated = true;
        packetActionUpdated = true;
        packetRestartAnim = true;
        packetExecuteShot = false;
    }

    public void update(LinkedHashSet<Integer> keyPresses, int delta) {
        super.updateLastPos();
        handleKeyPresses(keyPresses);
        float xy_multiplier;
        if (diagonal) {
            xy_multiplier = diagonalMultiplier;
        } else {
            xy_multiplier = 1;
        }
//        System.out.println("speedX: " + speedX * xy_multiplier);
        x += speedX * delta * xy_multiplier;
//        System.out.println("x: " + x); // + "; last_x: " + lastX);
        y += speedY * delta * xy_multiplier;
        if (currentAction == DEATH_FALL) {
            speedZ += 0.0015 * delta;
            z += speedZ * delta;
        }
        //0.85 is hardcoded
        crosshair_x += camMovedByX * 0.85 * Game.SCALE;
        crosshair_y += camMovedByY * 0.85 * Game.SCALE;
        //backup
//        crosshair_x -= (speedX * delta * xy_multiplier) * Game.SCALE - camMovedByX;
//        crosshair_y -= ((speedY * delta * xy_multiplier) / 2) * Game.SCALE - camMovedByY;


//        System.out.println((speedX * delta) * Game.SCALE);
//        System.out.println((speedX * delta) * Game.SCALE - (player_local_x - Game.SCALED_WIDTH / 2));
//        crosshair_x -= ((speedX * delta) * Game.SCALE + (player_local_x - Game.SCALED_WIDTH / 2) / delta);
//        crosshair_y -= ((speedY * delta) / 2) * Game.SCALE;
//        crosshair_x -= (speedX * delta - player_local_x - Game.SCALED_WIDTH / 2) * Game.SCALE;
//        crosshair_y -= ((speedY * delta - player_local_y - Game.SCALED_HEIGHT / 2) / 2) * Game.SCALE;
//        speedX /= Math.pow(dragForce, delta/10f); //geometric progression
//        speedY /= Math.pow(dragForce, delta/10f);
        speedX /= 1 + (dragForce * delta) * 0.1;
        speedY /= 1 + (dragForce * delta) * 0.1;
    }

    public void handleKeyPresses(LinkedHashSet<Integer> keyPresses) {
//        System.out.println("handling: " + keyPresses);
        if (!busy && alive) {
            off_x = 0;
            off_y = 0;
            for (Integer key : keyPresses) {
                if (key == Input.KEY_A) {
                    off_x -= moveSpeed;
                    left = true;
                    right = false;
                }
                else if (key == Input.KEY_D) {
                    off_x += moveSpeed;
                    right = true;
                    left = false;
                }
                else if (key == Input.KEY_W) {
                    off_y -= moveSpeed;
                    up = true;
                    down = false;
                }
                else if (key == Input.KEY_S) {
                    off_y += moveSpeed;
                    down = true;
                    up = false;
                }
            }
            if (moved()) { //if RAN by himself (this is important)
                float moveDistance = (float) Math.sqrt(Math.pow(off_x, 2) + Math.pow(off_y, 2));
                if (moveDistance > moveSpeed) {
                    diagonal = true;
//                    Not working due to progressive acceleration
//                    off_x = off_x / moveDistance * moveSpeed;
//                    off_y = off_y / moveDistance * moveSpeed;
                } else {
                    diagonal = false;
                }
                if (off_x != 0) {
//                    speedX = off_x;
                    speedX += off_x * accelerationMultiplier;
                    if (Math.abs(speedX) > moveSpeed) {
                        speedX = (float) (Math.signum(speedX) * moveSpeed);
                    }
                }
                if (off_y != 0) {
//                    speedY = off_y;
                    speedY += off_y * accelerationMultiplier;
                    if (Math.abs(speedY) > moveSpeed) {
                        speedY = (float) (Math.signum(speedY) * moveSpeed);
                    }
                }
            }
            updateDirection();
        }

        updateAction();

        left = false;
        right = false;
        up = false;
        down = false;
    }

    public void updateDirection() { //update facing direction based on changed coordinates
        if (left) {
            if (up) {
                direction = 5;
            } else if (down) {
                direction = 7;
            } else {
                direction = 6;
            }
        } else if (right) {
            if (up) {
                direction = 1;
            } else if (down) {
                direction = 3;
            } else {
                direction = 2;
            }
        } else if (up) {
            direction = 0;
        } else if (down){
            direction = 4;
        }
    }

    public void updateAction() {
        if (alive) {
            if (busy) {
                if (System.currentTimeMillis() - startedBeingBusyAt >= currentBusyTimer) {
                    if (currentAction == PREPARE_GUN) { //if the player was preparing gun and now its time to executeShot
                        executeShot();
                        return;
                    }
//                    setCurrentAction(IDLE, false);
                    busy = false;
                }
            } else if (moved()) {
                setCurrentAction(RUN, false);
            } else {
                setCurrentAction(IDLE, false);
            }
        }
    }

    public void setCurrentAction(int action, boolean restartAnim) {
        if (currentAction != ATTACK_SHOOT && currentAction != ATTACK_SLASH && currentAction == action) return; //mouse actions must be handled as soon as they come so that player sprite is displayed properly
        super.setCurrentAction(action, restartAnim);
        packetActionUpdated = true;
        packetRestartAnim = restartAnim;
    }

    public float slashAttack(int mouse_x, int mouse_y) {
        makeBusy(ATTACK_SLASH);
        setPlayerDirectionToMouse(mouse_x, mouse_y);
        float[] playerVector = getPlayerVectorToMouse(mouse_x, mouse_y);
        float x_ratio = playerVector[0];
        float y_ratio = playerVector[1];
        // recalc angle so that an actual topdown (and not screen-tilted) angle is returned and sent to the survur
        float angle = (float) Math.toDegrees(Math.atan(y_ratio/x_ratio)) + 90;
        if (Math.signum(x_ratio) == Math.signum(-1)) {
            angle += 180;
        }

        //change action and apply force
        setCurrentAction(ATTACK_SLASH, true);
//        System.out.println(x_ratio + " " + y_ratio);
        applyRawForce(x_ratio * sword_slash_dash_force, y_ratio * sword_slash_dash_force);
        return angle;
    }

    public void swordClash(float x_ratio, float y_ratio) {
        makeBusy(SWORD_CLASH);
        setCurrentAction(SWORD_CLASH, false);
        applyRawForce(x_ratio * sword_clash_force, y_ratio * sword_clash_force);
    }

    public void tumble(int mouse_x, int mouse_y) {
        if (System.currentTimeMillis() - previousTumbleWasAt < tumble_cooldown) return; //if yet on cooldown
        previousTumbleWasAt = System.currentTimeMillis();
        makeBusy(TUMBLE);
        setPlayerDirectionToMouse(mouse_x, mouse_y);
        setCurrentAction(TUMBLE, true);
        float[] playerVector = getPlayerVectorToMouse(mouse_x, mouse_y);
        float x_ratio = playerVector[0];
        float y_ratio = playerVector[1];
        applyRawForce(x_ratio * tumble_force, y_ratio * tumble_force);

    }

    public void shoot(int mouse_x, int mouse_y) {
        if (ammo == 0) return;
        crosshair_x = mouse_x;
        crosshair_y = mouse_y;
//        crosshair_x = lastX + (player_local_x - Game.SCALED_WIDTH / 2) / Game.SCALE + (mouse_x - Game.SCALED_WIDTH / 2) / Game.SCALE;
//        crosshair_x = lastX + (player_local_x - Game.SCALED_WIDTH / 2 + mouse_x - Game.SCALED_WIDTH / 2) / Game.SCALE;
//        crosshair_y = lastY + (player_local_y - Game.SCALED_HEIGHT / 2) / 2 / Game.SCALE + (mouse_y - Game.SCALED_HEIGHT / 2) * 2 / Game.SCALE;
//        crosshair_y = lastY + (player_local_y - Game.SCALED_HEIGHT / 2 + mouse_y - Game.SCALED_HEIGHT / 2) / Game.SCALE;
        makeBusy(PREPARE_GUN);
        setPlayerDirectionToMouse(mouse_x, mouse_y);
        setCurrentAction(PREPARE_GUN, true);
    }

    public void executeShot() {
        makeBusy(ATTACK_SHOOT);
        setCurrentAction(ATTACK_SHOOT, true);
        ammo--;
//        float localCrosshairX = Game.SCALED_WIDTH / 2 + (crosshair_x - x);
//        float localCrosshairY = Game.SCALED_HEIGHT / 2 + (crosshair_y - y) / 2;
        float[] playerVector = getPlayerVectorToMouse(crosshair_x, crosshair_y);
        setPlayerDirectionToMouse(crosshair_x, crosshair_y);
        shot[0] = x + player_local_x - Game.SCALED_WIDTH / 2;
        shot[1] = y + player_local_y - Game.SCALED_HEIGHT / 2;
        shot[2] = playerVector[0];
        shot[3] = playerVector[1];
        applyRawForce(-playerVector[0] * recoil_force, -playerVector[1] * recoil_force);
        doRenderShot = true;
        packetExecuteShot = true;
    }

    public float[] getPlayerVectorToMouse(float mouse_x, float mouse_y) {
        // Get the direction of the attack:
        // every game value must be scaled (for ex. player_height)
//        float distance_to_mouse_x = mouse_x - Game.SCALED_WIDTH / 2;
//        float distance_to_mouse_y = (mouse_y - (Game.SCALED_HEIGHT / 2 - player_height * Game.SCALE)) * 2; // *2 because of screen tilt
        float distance_to_mouse_x = mouse_x - player_local_x;
        float distance_to_mouse_y = (mouse_y - (player_local_y - player_height * Game.SCALE)) * 2; // *2 because of screen tilt

        float distance_to_mouse = (float) Math.sqrt(Math.pow(distance_to_mouse_x, 2) + Math.pow(distance_to_mouse_y, 2)); // or C in Pythagoras theo
        float x_ratio = distance_to_mouse_x / distance_to_mouse;
        float y_ratio = distance_to_mouse_y / distance_to_mouse;
        return new float[]{x_ratio, y_ratio};
    }

    public void setPlayerDirectionToMouse(float mouse_x, float mouse_y) {
//        float distance_to_mouse_x = mouse_x - Game.SCALED_WIDTH / 2;
//        float dir_distance_to_mouse_y = mouse_y - (Game.SCALED_HEIGHT / 2 - player_height * Game.SCALE); // Dir for player direction
//        System.out.println("Player local x: " + player_local_x);
        float distance_to_mouse_x = mouse_x - player_local_x;
        float dir_distance_to_mouse_y = mouse_y - (player_local_y - player_height * Game.SCALE); // Dir for player direction

        // calculate player new direction
        float distance_to_mouse = (float) Math.sqrt(Math.pow(distance_to_mouse_x, 2) + Math.pow(dir_distance_to_mouse_y, 2)); // or C in Pythagoras theo
        float x_ratio = distance_to_mouse_x / distance_to_mouse;
        float y_ratio = dir_distance_to_mouse_y / distance_to_mouse;
        float angle = (float) Math.toDegrees(Math.atan(y_ratio/x_ratio)) + 90;
        if (Math.signum(x_ratio) == Math.signum(-1)) {
            angle += 180;
        }
        int direction = (int) ((angle + 22.5) / 45);
        if (direction == 8) {direction = 0;}
        else if (direction == 7) {direction = 5;}
        else if (direction == 5) {direction = 7;}
        setDirection(direction);
    }

    public void makeBusy(int action) {
        busy = true;
        if (action == ATTACK_SLASH) {
            currentBusyTimer = bt_sword_slash;
        } else if (action == SWORD_CLASH) {
            currentBusyTimer = bt_sword_clash;
        } else if (action == PREPARE_GUN) {
            currentBusyTimer = bt_prepare_gun;
        } else if (action == ATTACK_SHOOT) {
            currentBusyTimer = bt_shoot;
        } else if (action == TUMBLE) {
            currentBusyTimer = bt_tumble;
        } else {
            currentBusyTimer = 0;
        }
        startedBeingBusyAt = System.currentTimeMillis();
    }

    public void die(int died_by) {
        if (!alive) return;
        if (died_by == DEATH_BY_FALLING_OFF) {
            setCurrentAction(DEATH_FALL, false);
            dragForce = 0.01f;
            died_at_x = x;
            died_at_y = y;
        } else {
            setCurrentAction(DEATH_KILLED, true);
        }
        alive = false;
        packetAliveUpdated = true;
    }

    public void rise() {
        if (alive) return;
        alive = true;
        packetAliveUpdated = true;
//        setCurrentAction(IDLE, false);
        speedX = 0;
        speedY = 0;
        speedZ = 0;
        dragForce = DEFAULT_DRAG_FORCE;
    }
    public void applyForce(float x, float y) { //dont forget to use delta
        speedX += x;
        speedY += y;
    }

    public void applyRawForce(float x, float y) { //dont forget to use delta
        speedX = x;
        speedY = y;
    }

    public boolean moved() {
        return (left || right || up || down) && (off_x != 0 || off_y != 0);
    }

    public boolean isBusy() {
        return busy;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isReady() {
        return ready;
    }

    public void toggleReady() {
        ready = !ready;
    }

    public float[] getShot() {
        return shot;
    } //not telling the player to get shot, but to retrieve the shot info

    public float getCrosshairX() {
        return crosshair_x;
    }

    public float getCrosshairY() {
        return crosshair_y;
    }

    public float getDied_at_x() {
        return died_at_x;
    }

    public float getDied_at_y() {
        return died_at_y;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public void setPlayer_local_x(float player_local_x) {
        this.player_local_x = player_local_x;
    }

    public void setPlayer_local_y(float player_local_y) {
        this.player_local_y = player_local_y;
    }

    public void setCamMovedByX(float camMovedByX) {
        this.camMovedByX = camMovedByX;
    }

    public void setCamMovedByY(float camMovedByY) {
        this.camMovedByY = camMovedByY;
    }

    public boolean wasAliveUpdated() {
        return packetAliveUpdated;
    }

    public void packetAliveUpdateSent() {
        packetAliveUpdated = false;
    }

    public boolean wasActionUpdated() {
        return packetActionUpdated;
    }

    public boolean wasAnimRestarted() {
        return packetRestartAnim;
    }

    public boolean wasShotExecuted() {
        return packetExecuteShot;
    }

    public void packetUpdateActionSent() {
        packetActionUpdated = false;
        packetRestartAnim = false;
    }

    public void packetExecuteShotSent() {
        packetExecuteShot = false;
    }

}
