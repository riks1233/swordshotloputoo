package Networking;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ClientProgram extends Listener {

    private Client client;
//    private static String ip = "localhost";
//    private int port = 27960;
//    private ArrayList<Object> receivedPackets;
    private ConcurrentLinkedQueue<Object> receivedPackets;

    public void connect(String ip, int port) throws Exception {
        client = new Client();
        Network.register(client);

        receivedPackets = new ConcurrentLinkedQueue<>();
        client.addListener(this);

        client.start();
        try {
            client.connect(5000, ip, port); //throws exception
        } catch (Exception e) {
            client.stop();
            throw e; //throw further into the InGame
        }
    }

    public ConcurrentLinkedQueue<Object> getReceivedPackets() {
        return receivedPackets;
//        return receivedPackets;
    } //TODO: remake to use queue instead of cloning arraylist because it may cause packet drops (clearing a list containing one new received packet)

//    public void clearReceivedPackets() {
//        receivedPackets.clear();
//    }

    public void received(Connection c, Object o){
        receivedPackets.add(o);
    }

    public Client getClient() {
        return client;
    }
}
