package Entity;

import org.newdawn.slick.Graphics;

public interface GameObject {
    public float getY();
    public void render(Graphics g, float interpolation);
}
