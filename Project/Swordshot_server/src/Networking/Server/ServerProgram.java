package Networking.Server;

import Networking.Network;
import Networking.Network.*; //import all packets

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ServerProgram extends Listener{

    //    public static final int port = 80;
    public ServerLoop serverLoop;
    public Server server;
    public ConcurrentLinkedQueue<ServerReceivedPacket> receivedPackets;

    public ServerProgram() {
        server = new Server();
        serverLoop = new ServerLoop(this, server);
        receivedPackets = new ConcurrentLinkedQueue<>();
    }

    public static void main(String[] args) throws IOException {
        ServerProgram sp = new ServerProgram();
        Network.register(sp.server);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the port number that server must run at (default is 27960): ");
        int port = 27960;
        try {
            String portString = br.readLine();
            if (!portString.equals("")) port = Integer.parseInt(portString);
        } catch (Exception e) {
            System.out.println("Something went wrong while reading the port number, using default port 27960");
        }
        br.close();
        sp.server.bind(port); // throws ex
        sp.server.addListener(sp);
        sp.serverLoop.start();
        sp.server.start();

        System.out.println("The server is running. Opened at port " + port);
    }

    public void connected(Connection c){
        serverLoop.connected(c);
    }

    public void disconnected(Connection c){
        serverLoop.disconnected(c);
    }

    public void received(Connection c, Object o){
        receivedPackets.add(new ServerReceivedPacket(c, o));
    }

    public ConcurrentLinkedQueue<ServerReceivedPacket> getReceivedPackets() {
        return receivedPackets;
    }
}
