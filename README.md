# README #
This is a project called Swordshot - a little game made to be played through lan by two players who oppose each other.

# Running the game and server applications: #
For the released and runnable Windows versions, download the "Release" folder from the repository.

Server:

Open the server folder: Release/SwordShotServer/

Run the batch file "run_swordshot_server.bat". This will run server "Swordshot_server.jar" file and open its command prompt window.

In the opened command prompt window, specify the port number at which the server must be ran at as prompted. Press "Enter" key.

The server must be up and running.

Game:

Open the server folder: Release/SwordShotGameClient/

Run the batch file "run_swordshot.bat". This will run the game.

To connect to the server, specify the IP address and port number at which the server application is running. Press "Enter" key.

The game client should connect to the server.