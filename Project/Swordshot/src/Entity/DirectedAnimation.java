package Entity;

import Main.Game;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class DirectedAnimation {

    public static final double frameDuration = 16.667; //time meant for 1 frame (60 fps means one frame has 16.666 ms per second to be displayed)
    public static final int rows = 5;

    private Image[] frames; //informally 2-axis array: say, if spritesheet has 5 rows (representing directions) and 10 frames in a row,
    // then frames[direction * animationLength + currentFrame] gives us the needed frame with selected direction
    private int animationLength;
    private int[] frameDurations;
    private int currentFrame;
    private int anchor_x;
    private int anchor_y;


    private long startTime;
    private boolean playedOnce;
    private boolean repeatable;

    public DirectedAnimation(String src, boolean repeatable, int[] frameDurations, int width, int height, int anchor_x, int anchor_y) {
        this.anchor_x = anchor_x;
        this.anchor_y = anchor_y;
        this.frameDurations = frameDurations;
        this.playedOnce = false;
        this.repeatable = repeatable;
        animationLength = frameDurations.length;
        try {
            Image spritesheet = new Image(src);
            frames = new Image[animationLength * rows];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < animationLength; j++) {
                    Image subImg = spritesheet.getSubImage(j * width, i * height, width, height);
                    subImg.setFilter(Image.FILTER_NEAREST); // so that scaling does not blur pixels
                    frames[i * animationLength + j] = subImg;
                }
            }

        } catch (SlickException e) {
            e.printStackTrace();
        }
        restart();

    }

    public Image[] getFrames() {
        return frames;
    }

    public void restart() {
        playedOnce = false;
        currentFrame = 0;
        startTime = System.nanoTime();
    }

    public void render() {
        if (repeatable || !playedOnce) {
            long elapsed = (System.nanoTime() - startTime) / 1000000;
            if (elapsed > frameDuration * frameDurations[currentFrame]) {
                currentFrame++;
                startTime = System.nanoTime();
            }
            if (currentFrame >= animationLength) {
                if (repeatable) {
                    currentFrame = 0;
                } else {
                    currentFrame = animationLength - 1;
                }
                playedOnce = true;
            }
        } else {
            currentFrame = animationLength - 1;
        }
    }

    public void setRepeatable(boolean repeatable) {
        this.repeatable = repeatable;
    }

    public Image getImage(int direction) {
        if (direction > 4) {
            direction -= 4;
        }
        return frames[direction * animationLength + currentFrame]; }

    public boolean hasPlayedOnce() { return playedOnce; }

    public int getAnchor_x() {
        return anchor_x;
    }

    public int getAnchor_y() {
        return anchor_y;
    }
}