package Networking.Server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;
import Networking.Network.*;
import org.lwjgl.Sys;
import org.newdawn.slick.geom.*;
import sun.security.provider.SHA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ServerLoop implements Runnable {

    public static final int UPS = 20; //network updates
    public static final double UPDATE_INTERVAL_NS = 1000000000 / UPS;
    public static final int GAME_START_DELAY_MS = 5000;
    public static final int GAME_END_DELAY_MS = 10000;

    public static final int WINNING_SCORE = 20;

    public static final int STATE_LOBBY = 0;
    public static final int STATE_COUNTDOWN = 1;
    public static final int STATE_INGAME = 2;
    public static final int STATE_ENDGAME = 3;

    public static final int ATTACK_SLASH = 2;
    public static final int ATTACK_SHOOT = 3;
    public static final int SWORD_ATTACK_LENGTH = 85;
    //    public static final int SWORD_ATTACK_LENGTH = 300; //testing
    public static final int SWORD_ATTACK_WIDTH = 20;
    //TODO: check sword clash online
    public static final int SWORD_CLASH_POSSIBILITY_UPDATE_INTERVAL = 4; // how many updates there can be before attacked player can answer by a swordSlash and thus create a sword clash
    //    public static final int SWORD_CLASH_POSSIBILITY_UPDATE_INTERVAL = 120; //testing
    public static final int SHOT_DISTANCE = 2000;
    public static final int PLAYER_SWORD_HITBOX_WIDTH = 18;
    public static final int PLAYER_GUN_HITBOX_WIDTH_RADIUS = 7;
    public static final int PLAYER_GUN_HITBOX_HEIGHT_RADIUS = 18;
    public static final int PLAYER_DEATH_TIMER_MS = 2000;
    public static final int map_size = 12;
    public static final int tile_size = 100;
    public static final int map_height = tile_size * map_size;
    public static final int map_middle_point = map_height / 2;
    public static final int[][] ingame_spawn_locations = new int[][]{
            new int[]{0, 150},
            new int[]{-map_middle_point + 150, map_middle_point},
            new int[]{map_middle_point - 150, map_middle_point},
            new int[]{0, map_height - 150}};
    public static final int[][] lobby_spawn_locations = new int[][]{
            new int[]{0, map_middle_point - 200},
            new int[]{- 200, map_middle_point},
            new int[]{200, map_middle_point},
            new int[]{0, map_middle_point + 200}};
    public static final int MAX_PLAYERS = 2;
    private int currentState;
    private int prevCountdownSecond;
    private long gameTimeoutTimer;
//    public static final int[][] spawn_locations = new int[][]{new int[]{0, 100}}; //debugging

    private ServerProgram sp;
    private Server server;

    private Thread thread;
    private boolean running = false;
    private int actual_ups;
    private int serverUptime;

    private Map<Integer, Player> players;
    private ArrayList<int[]> playerSuccessfulSwordAttacks; //[0] - attackingPlayerID, [1] - attackedPlayerID,
    //    private ArrayList<Player> deadPlayers; // server side dead players
    // [2] - attackingPlayerX, [3] - attackingPlayerY
    // [4] - sword_clash_update_interval_countdown (if reaches 0 - execute the successfull attack),
    public ServerLoop(ServerProgram sp, Server server) {
        this.sp = sp;
        this.server = server;
        players = new HashMap<>();
        playerSuccessfulSwordAttacks = new ArrayList<>();
        currentState = STATE_LOBBY;
//        deadPlayers = new ArrayList<>();
    }

    public void start() {
        running = true;
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    public void connected(Connection c){ //handle this immediately
        System.out.println("Connection received.");
        if (currentState != STATE_LOBBY) {
            kickPlayer(c, "game in progress");
            return;
        } else if (players.size() == MAX_PLAYERS) {
            //send message of server full
            kickPlayer(c, "server full");
            return;
        }
        Player player = new Player();
        player.c = c;
        player.alive = false;
        player.respawnPacketSent = false;
        player.ready = false;
        respawnPlayer(player);
        updatePlayerScore(player, 0);
        PacketMessage packet_m = new PacketMessage();
        packet_m.message = "Waiting for other players ...";
        c.sendTCP(packet_m);
//        setPlayerPosition(player, 0, 0);

        PacketAddPlayer packet_addNewPlayer = new PacketAddPlayer();
        packet_addNewPlayer.id = c.getID();
        packet_addNewPlayer.x = player.x;
        packet_addNewPlayer.y = player.y;
        server.sendToAllExceptTCP(c.getID(), packet_addNewPlayer); //inform others that a player has connected

        //inform this player about other players
        for(Player p : players.values()){
            PacketAddPlayer packet_addPlayer = new PacketAddPlayer();
            packet_addPlayer.id = p.c.getID();
            packet_addPlayer.x = p.x;
            packet_addPlayer.y = p.y;
            c.sendTCP(packet_addPlayer);
        }

        players.put(c.getID(), player);
        checkLobbyStatus();
    }

    public void disconnected(Connection c){ //handle this immediately
        Player removed = players.remove(c.getID());
        if (removed == null) { //if we removed the kicked-while-connecting player
            return;
        }
        PacketRemovePlayer packet = new PacketRemovePlayer();
        packet.id = c.getID();
        server.sendToAllExceptTCP(c.getID(), packet);
        System.out.println("Connection dropped.");

        if (currentState == STATE_COUNTDOWN || players.size() == 0) {
            enterLobbyMode();
        } else {
            checkLobbyStatus();
        }
        checkForTechnicalWin();
    }

    @Override
    public void run() {
        long timer = System.currentTimeMillis();
        long now;
        double lastUpdateTime = System.nanoTime();
        serverUptime = 0;

        actual_ups = 0;
        int ups_counter = 0;

        while (running) {
            now = System.nanoTime();

            while(now - lastUpdateTime > UPDATE_INTERVAL_NS) { //potentially handling catchups
                update();
                ups_counter++;
                lastUpdateTime += UPDATE_INTERVAL_NS;
            }

            if (System.currentTimeMillis() - timer >= 1000) {
                actual_ups = ups_counter;
//                System.out.println(actual_ups);
//                System.out.println(deadPlayers);
                ups_counter = 0;
                timer += 1000;
                serverUptime += 1;
            }

            while (now - lastUpdateTime < UPDATE_INTERVAL_NS) {
                Thread.yield();
                try {Thread.sleep(1);} catch(Exception e) {e.printStackTrace();}
                now = System.nanoTime();
            }
        }
    }

    public void update() {
        handlePackets();
        if (currentState == STATE_LOBBY && players.size() == MAX_PLAYERS) { //if in lobby and server is filled
            boolean startCountdown = true;
            for (Player p : players.values()) {
                if (p.ready == false) {
                    startCountdown = false;
                    break;
                }
            }
            if (startCountdown) {
                System.out.println("Countdown started");
                startCountdown();
            }

        } else if (currentState == STATE_COUNTDOWN) {
            if (gameTimeoutTimer > System.currentTimeMillis()) {
                int newSecond = (int) Math.ceil((gameTimeoutTimer - System.currentTimeMillis()) / 1000.0);
                if (prevCountdownSecond != newSecond) {
                    prevCountdownSecond = newSecond;
                    PacketMessage packet_m = new PacketMessage();
                    packet_m.message = "Starting in: " + newSecond;
                    server.sendToAllTCP(packet_m);
                }
            } else {
                PacketMessage packet_m = new PacketMessage();
                packet_m.message = "";
                System.out.println("Game started");
                startGame();
                server.sendToAllTCP(packet_m);
            }
        } else if (currentState == STATE_ENDGAME) {
            if (System.currentTimeMillis() >= gameTimeoutTimer) {
                enterLobbyMode();
            }
        }
        if (!playerSuccessfulSwordAttacks.isEmpty()) { //if there was a succesfull attack then check it to be countered or execute it
            for (int i = 0; i < playerSuccessfulSwordAttacks.size(); i++) {
                int[] attack = playerSuccessfulSwordAttacks.get(i);
                // [0] - attackingPlayerID, [1] - attackedPlayerID,
                // [2] - attackingPlayerX, [3] - attackingPlayerY
                // [4] - sword_clash_update_interval_countdown (if reaches 0 - execute the successfull attack),
                if (attack[4] == 0) {
                    // execute successful attack and kill the other player
                    killPlayer(players.get(attack[1]));
                    Player attackingPlayer = players.get(attack[0]);
                    updatePlayerScore(attackingPlayer, attackingPlayer.score + 1);
                    playerSuccessfulSwordAttacks.remove(attack);
                    i--;
                } else {
                    attack[4]--;
                }
            }
        }
//        if (!deadPlayers.isEmpty()) {
//            for (int i = 0; i < deadPlayers.size(); i++) {
//                Player p = deadPlayers.get(i);
//                if (System.currentTimeMillis() - deadPlayers.get(i).diedOn >= PLAYER_DEATH_TIMER_MS) {
//                    respawnPlayer(p);
//                    deadPlayers.remove(p);
//                    i--;
//                }
//            }
//        }
        for (Player p : players.values()) {
            if (!p.alive) {
//                System.out.println(p.c.getID() + " " + (System.currentTimeMillis() - p.diedOn));
                if (System.currentTimeMillis() - p.diedOn >= PLAYER_DEATH_TIMER_MS) {
                    respawnPlayer(p);
                }
            }
        }
    }

    public void handlePackets() {
        ConcurrentLinkedQueue<ServerReceivedPacket> receivedPackets = sp.getReceivedPackets();
        ServerReceivedPacket srp;
        while ((srp = receivedPackets.poll()) != null) {
            int cid = srp.c.getID();
            if (!players.containsKey(cid)) continue; //if a player was removed in the process
            Object o = srp.o;
            if(o instanceof PacketUpdateX){
                PacketUpdateX packet = (PacketUpdateX) o;
                players.get(cid).x = packet.x;
                packet.id = cid;
                server.sendToAllExceptTCP(cid, packet);
//            System.out.println("received and sent an update X packet");
            } else if(o instanceof PacketUpdateY){
                PacketUpdateY packet = (PacketUpdateY) o;
                players.get(cid).y = packet.y;
                packet.id = cid;
                server.sendToAllExceptTCP(cid, packet);
//            System.out.println("received and sent an update Y packet");
            } else if(o instanceof PacketUpdateZ){
                PacketUpdateZ packet = (PacketUpdateZ) o;
                players.get(cid).z = packet.z;
                packet.id = cid;
                server.sendToAllExceptTCP(cid, packet);
//            System.out.println("received and sent an update Y packet");
            } else if(o instanceof PacketUpdateDirection){
                PacketUpdateDirection packet = (PacketUpdateDirection) o;
                packet.id = cid;
                server.sendToAllExceptTCP(cid, packet);
            } else if(o instanceof PacketUpdateAction){
                PacketUpdateAction packet = (PacketUpdateAction) o;
                packet.id = cid;
                server.sendToAllExceptTCP(cid, packet);
            } else if(o instanceof PacketAttackSlash){
                PacketAttackSlash packet = (PacketAttackSlash) o;
                playerSlash(cid, packet.angle);
            } else if(o instanceof PacketAttackShoot){
                PacketAttackShoot packet = (PacketAttackShoot) o;
                playerShoot(cid, packet.shot);
            } else if(o instanceof PacketUpdateAlive){
                PacketUpdateAlive packet = (PacketUpdateAlive) o;
                Player p = players.get(cid);
                p.alive = packet.alive;
                if (!p.alive) checkPlayerCrossedBoundary(p);
            } else if(o instanceof PacketReady){
                if (currentState == STATE_LOBBY && players.size() == MAX_PLAYERS) {
                    PacketReady packet_ready = new PacketReady();
                    Player p = players.get(cid);
                    p.ready = !p.ready;
                    p.c.sendTCP(packet_ready);
                }
            }
        }
    }

    public void playerSlash(int cid, float angle) {
        Player attackingPlayer = players.get(cid);
        if (!attackingPlayer.alive) return;
//        if (deadPlayers.contains(attackingPlayer)) return;
//        System.out.println(angle);
        //here we are creating a point array for a rectangle that lays on the floor as a sword hitbox facing up (player direction 0 (check Entity.Player class)) from the player's legs
        float[] points = new float[]{
                attackingPlayer.x - SWORD_ATTACK_WIDTH/2, attackingPlayer.y,
                attackingPlayer.x - SWORD_ATTACK_WIDTH/2, attackingPlayer.y - SWORD_ATTACK_LENGTH,
                attackingPlayer.x + SWORD_ATTACK_WIDTH/2, attackingPlayer.y - SWORD_ATTACK_LENGTH,
                attackingPlayer.x + SWORD_ATTACK_WIDTH/2, attackingPlayer.y
        };
        //creating the rectangle
        Shape attackHitbox = new Polygon(points);
        //getting the degrees from player's attack and rotating the sword hitbox as needed.
        attackHitbox = attackHitbox.transform(Transform.createRotateTransform((float) Math.toRadians(angle), attackingPlayer.x, attackingPlayer.y));
        //check for all players if they have been attacked
        for (Player p : players.values()) {
            if (p.c.getID() == cid || !p.alive) continue;
            Shape playerHitbox = new Polygon(new float[]{
                    p.x - PLAYER_SWORD_HITBOX_WIDTH / 2, p.y - PLAYER_SWORD_HITBOX_WIDTH / 2,
                    p.x + PLAYER_SWORD_HITBOX_WIDTH / 2, p.y - PLAYER_SWORD_HITBOX_WIDTH / 2,
                    p.x - PLAYER_SWORD_HITBOX_WIDTH / 2, p.y + PLAYER_SWORD_HITBOX_WIDTH / 2,
                    p.x + PLAYER_SWORD_HITBOX_WIDTH / 2, p.y + PLAYER_SWORD_HITBOX_WIDTH / 2
            });

            // debugging
//            DebugAttack shapePacket = new DebugAttack();
//            shapePacket.shape = attackHitbox;
//            server.sendToTCP(cid, shapePacket);
//            DebugAttack shapePacket2 = new DebugAttack();
//            shapePacket2.shape = playerHitbox;
//            server.sendToTCP(cid, shapePacket2);
            if (attackHitbox.intersects(playerHitbox) || attackHitbox.contains(playerHitbox)) {
                System.out.println("PLAYER " + cid + " HIT TARGET: " + p.c.getID());
                for (int i = 0; i < playerSuccessfulSwordAttacks.size(); i++) {
                    int[] attack = playerSuccessfulSwordAttacks.get(i);
                    //check if this player was previously attacked by the player this player currently attacked (could not phrase it better :(, sounds stupid)
                    if (attack[0] == p.c.getID() && attack[1] == cid) {
                        // execute sword clash
                        // [0] - attackingPlayerID, [1] - attackedPlayerID,
                        // [2] - attackingPlayerX, [3] - attackingPlayerY
                        // [4] - sword_clash_update_interval_countdown (if reaches 0 - execute the successfull attack),
                        swordClash(attack[0], attack[2], attack[3], cid, (int) attackingPlayer.x, (int) attackingPlayer.y);
                        playerSuccessfulSwordAttacks.remove(attack);
                        return;
//                        i--;
                    }
                }
                playerSuccessfulSwordAttacks.add(new int[]{cid, p.c.getID(), (int) attackingPlayer.x, (int) attackingPlayer.y, SWORD_CLASH_POSSIBILITY_UPDATE_INTERVAL});
            }

        }
    }

    public void playerShoot(int cid, float[] shot) {
        Player attackingPlayer = players.get(cid);
        if (!attackingPlayer.alive) return;
//        if (deadPlayers.contains(attackingPlayer)) return;
        //creating the rectangle
        float[] shotLine = new float[]{shot[0], shot[1], shot[0] + shot[2] * SHOT_DISTANCE, shot[1] + shot[3] * SHOT_DISTANCE};
//        float[] shotLine = new float[]{shot[0], shot[1], 300,300};
        Shape attackHitbox = new Polygon(shotLine);
        //getting the degrees from player's attack and rotating the sword hitbox as needed.
        //check for all players if they have been attacked
        for (Player p : players.values()) {
            if (p.c.getID() == cid || !p.alive) continue;
//            Shape playerHitbox = new Polygon(new float[]{
//                    p.x - PLAYER_HITBOX_WIDTH / 2, p.y - PLAYER_HITBOX_WIDTH / 2 - PLAYER_HITBOX_HEIGHT,
//                    p.x + PLAYER_HITBOX_WIDTH / 2, p.y - PLAYER_HITBOX_WIDTH / 2 - PLAYER_HITBOX_HEIGHT,
//                    p.x - PLAYER_HITBOX_WIDTH / 2, p.y + PLAYER_HITBOX_WIDTH / 2 + PLAYER_HITBOX_HEIGHT,
//                    p.x + PLAYER_HITBOX_WIDTH / 2, p.y + PLAYER_HITBOX_WIDTH / 2 + PLAYER_HITBOX_HEIGHT
//            });
            Shape playerHitbox = new Ellipse(p.x, p.y, PLAYER_GUN_HITBOX_WIDTH_RADIUS , PLAYER_GUN_HITBOX_HEIGHT_RADIUS * 2);

            // debugging
//            DebugAttack shapePacket = new DebugAttack();
//            shapePacket.shape = attackHitbox;
//            server.sendToTCP(cid, shapePacket);
//            DebugAttack shapePacket2 = new DebugAttack();
//            shapePacket2.shape = playerHitbox;
//            server.sendToTCP(cid, shapePacket2);
            if (attackHitbox.intersects(playerHitbox)) {
                System.out.println("PLAYER " + cid + " SHOT TARGET: " + p.c.getID());
                killPlayer(p);
                updatePlayerScore(attackingPlayer, attackingPlayer.score + 1);
            }
        }

        PacketAttackShoot packet_shot = new PacketAttackShoot();
        packet_shot.id = cid;
        packet_shot.shot = shot;
        server.sendToAllExceptTCP(cid, packet_shot);
    }

    public void setPlayerPosition(Player p, float x, float y) {
        PacketSetPlayerPosition packet = new PacketSetPlayerPosition();
        p.x = x;
        p.y = y;
        packet.x = x;
        packet.y = y;
        p.c.sendTCP(packet);
    }

    public void startCountdown() {
        System.out.println("Counting down.");
        currentState = STATE_COUNTDOWN;
        gameTimeoutTimer = System.currentTimeMillis() + GAME_START_DELAY_MS;
        prevCountdownSecond = 0;
//        PacketReady packet_ready = new PacketReady();
        for (Player p : players.values()) {
            killPlayer(p);
            p.diedOn += GAME_START_DELAY_MS;
            updatePlayerScore(p, 0);
            p.ready = false;
//            p.c.sendTCP(packet_ready);
        }
    }

    public void startGame() {
        System.out.println("Entered start game mode.");
        currentState = STATE_INGAME;
        for (Player p : players.values()) {
            respawnPlayer(p);
        }
    }

    public void endGame(Player winningPlayer) {
        if (currentState != STATE_INGAME) return; // if we are in lobby we cannot win
        System.out.println("Entered end game mode.");
        currentState = STATE_ENDGAME;
        gameTimeoutTimer = System.currentTimeMillis() + GAME_END_DELAY_MS;
        winningPlayer.respawnPacketSent = false;
        PacketMessage packet_m = new PacketMessage();
        packet_m.message = "YOU HAVE WON!";
        winningPlayer.c.sendTCP(packet_m);
        packet_m.message = "Player " + winningPlayer.c.getID() + " has won!";
        for (Player p : players.values()) {
            if (p.equals(winningPlayer)) {

            } else {
                killPlayer(p);
                p.diedOn += GAME_END_DELAY_MS;
                p.c.sendTCP(packet_m);
            }
        }
    }

    public void enterLobbyMode() {
        //return to lobby mode
        if (currentState == STATE_LOBBY) return;
        System.out.println("Entered lobby mode.");
        currentState = STATE_LOBBY;
        for (Player p : players.values()) {
            respawnPlayer(p);
            updatePlayerScore(p, 0);
        }
        checkLobbyStatus();
    }

    public void checkPlayerCrossedBoundary(Player p) {
        //as the map is a rotated rectangle starting from (0,0) - kehtib:
        float checkY;
        if (p.y > map_middle_point) {
            checkY = map_height - p.y;
        } else {
            checkY = p.y;
        }
        if (p.x < -checkY || p.x > checkY) {
            System.out.println("player is out of bounds");
            killPlayer(p);
            updatePlayerScore(p, p.score - 1);
        }
    }

    public void checkLobbyStatus() {
        System.out.println("Checked lobby status");
        if (currentState == STATE_LOBBY) {
            PacketMessage packet_m = new PacketMessage();
            if (players.size() == MAX_PLAYERS) {
                packet_m.message = "Lobby: Press R to get ready";
            } else {
                packet_m.message = "Lobby: Waiting for other players ...";
            }
            for (Player p : players.values()) { //drop ready values of already "pressed ready" players
                if (p.ready) {
                    PacketReady packet_ready = new PacketReady();
                    p.ready = false;
                    p.c.sendTCP(packet_ready);
                }
            }
            server.sendToAllTCP(packet_m);
        }
    }

    public void checkForTechnicalWin() {
        if (currentState == STATE_INGAME && players.size() == 1) {
            for (Player p : players.values()) {
                endGame(p);
                return;
            }
        }
    }

    public void swordClash(int cid1, int p1_x, int p1_y, int cid2, int p2_x, int p2_y) {
        int dx = p2_x - p1_x;
        int dy = p2_y - p1_y;
        float c = (float) Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        if (c == 0.0) { //if players were on the same spot, when attacked eachother (because division by 0.0 gives Infinity)
            return;
        }
        // these are ratios for the second player. Ratios for 1st player are inversed.
        float x_ratio = dx / c;
        float y_ratio = dy / c;
        PacketSwordClash packet1 = new PacketSwordClash();
        PacketSwordClash packet2 = new PacketSwordClash();
        packet1.x_ratio = -x_ratio;
        packet1.y_ratio = -y_ratio;
        packet2.x_ratio = x_ratio;
        packet2.y_ratio = y_ratio;
        server.sendToTCP(cid1, packet1);
        server.sendToTCP(cid2, packet2);
    }

    public void updatePlayerScore(Player p, int newScore) {
        if (currentState == STATE_ENDGAME || (currentState == STATE_LOBBY && newScore != 0)) return;
        if (newScore >= WINNING_SCORE) {
            endGame(p);
        }
        p.score = newScore;
        PacketUpdateScore packet_score = new PacketUpdateScore();
        packet_score.score = p.score;
        p.c.sendTCP(packet_score);
    }

    public void killPlayer(Player p) {
        System.out.println("killedSOME");
        PacketKillPlayer packet = new PacketKillPlayer();
        server.sendToTCP(p.c.getID(), packet);
        p.diedOn = System.currentTimeMillis();
        p.respawnPacketSent = false;
        p.alive = false;
//        deadPlayers.add(p);
    }

    public void respawnPlayer(Player resp_p) {
        if (resp_p.respawnPacketSent) return;
        PacketRespawnPlayer packet = new PacketRespawnPlayer();
        int[][] chosen_spawn_locations;
        if (currentState == STATE_INGAME) {
            chosen_spawn_locations = ingame_spawn_locations.clone();
            packet.ammo = 1;
        } else {
            chosen_spawn_locations = lobby_spawn_locations.clone();
            packet.ammo = 99;
        }
        ArrayList<int[]> respawnPositions = new ArrayList<>();
        for (int[] i : chosen_spawn_locations) {
            respawnPositions.add(i);
        }
        //make sure the spawn location is chosed in a such way that player cannot be spawned near another player
        int[] newPosition;
        if (players.size() >= respawnPositions.size()) { //if we have more players on the server than there is respawn locations - respawn randomly
            newPosition = respawnPositions.get(randomWithRange(0, respawnPositions.size() - 1));
        } else {
            for (Player p : players.values()) {
                if (p.equals(resp_p)) {
                    continue;
                }
                int[] closest = respawnPositions.get(0);
                int[] playerPos = new int[]{(int) p.x, (int) p.y};
                for (int[] pos : respawnPositions) {
                    if (isPosCloserThan(pos, closest, playerPos)) {
                        closest = pos;
                    }
                }
//                for (int asd : closest) {
//                    System.out.println(asd);
//                }
//                System.out.println(closest);
                respawnPositions.remove(closest);
            }
//            System.out.println("respposes: " + respawnPositions);
            newPosition = respawnPositions.get(randomWithRange(0, respawnPositions.size() - 1));
        }


        resp_p.x = newPosition[0]; //does not really change player's position because player defines his position, but helps to decide other player positions if we respawn more than one player at a time
        resp_p.y = newPosition[1];
        packet.x = resp_p.x;
        packet.y = resp_p.y;
        server.sendToTCP(resp_p.c.getID(), packet);
        resp_p.respawnPacketSent = true;
    }

    public void kickPlayer(Connection c, String message) {
        PacketKick kick_packet = new PacketKick();
        kick_packet.message = message;
        c.sendTCP(kick_packet);
        c.close();
        System.out.println("Kicked a player");
    }

    public static boolean isPosCloserThan(int[] newPos, int[] oldPos, int[] targetPos) {
        float newDistance = (float) Math.sqrt(Math.pow(targetPos[0] - newPos[0], 2) + Math.pow(targetPos[1] - newPos[1], 2));
        float oldDistance = (float) Math.sqrt(Math.pow(targetPos[0] - oldPos[0], 2) + Math.pow(targetPos[1] - oldPos[1], 2));
        if (newDistance < oldDistance) {
            return true;
        }
        return false;
    }

    public static int randomWithRange(int min, int max) //max is inclusive
    {
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }

    public class Player {
        public float x, y, z;
        public Connection c;
        public int score;
        public boolean alive; // player side alive status
        public boolean respawnPacketSent;
        public long diedOn; //death timer
        public boolean ready;
    }
}
