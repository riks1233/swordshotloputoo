package Main;

import Networking.Network;
import Networking.Server.ServerProgram;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import java.io.IOException;
//start server with 1 client
public class Main {
    public static void main(String[] args) throws IOException, SlickException {
        ServerProgram sp = new ServerProgram();
        Network.register(sp.server);
        sp.server.bind(27960); // throws ex
        sp.server.addListener(sp); //
        sp.serverLoop.start();
        sp.server.start();

        System.out.println("The server is ready");
        AppGameContainer appgc1;
        appgc1 = new AppGameContainer(new Game("Swordshot"), Game.SCALED_WIDTH, Game.SCALED_HEIGHT, false);
        appgc1.setUpdateOnlyWhenVisible(false);
        appgc1.setAlwaysRender(true);
        appgc1.start();


    }
}
