package Main;

import javax.swing.JFrame;
import GameState.Menu;
import GameState.InGame;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class Game extends StateBasedGame{
    public static final int WIDTH = 640;
    public static final int HEIGHT = 360;
    public static int SCALE = 2;
    public static int SCALED_WIDTH = 640 * SCALE;
    public static int SCALED_HEIGHT = 360 * SCALE;
    public static final int MOUSE_HOTSPOT_X = 1; //for some reason mouse hotspot values are always doubled when setting a new mouse cursor :( (value 5 would put cursor hotspot at 10)
    public static final int MOUSE_HOTSPOT_Y = 1;

    public static final int STATE_MENU = 0;
    public static final int STATE_INGAME = 1;

    public static final String sword_cursor_ref = "res/cursor/cursor1.png";
    public static final String crosshair_cursor_ref = "res/cursor/cursor1.png";
    public static final  int[] sword_cursor_hotspots = new int[]{1,1};
    public static final  int[] crosshair_cursor_hotspots = new int[]{5,5};


    public Game(String name) {
        super(name);
    }

    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {
        addState(new Menu(STATE_MENU)); // First state added is automatically entered (its enter() method is called)
        addState(new InGame(STATE_INGAME));
    }

    public static void main(String[] args) {
        try {
            AppGameContainer appgc = new AppGameContainer(new Game("Swordshot"), SCALED_WIDTH, SCALED_HEIGHT, false);
            appgc.setUpdateOnlyWhenVisible(false);
            appgc.setAlwaysRender(true);
//            appgc.setFullscreen(true); // only works if the resolution of the game container is set to the device's available full screen resolution so use this class' method setFullscreen instead
//            setFullscreen(appgc);
//            appgc.setMouseGrabbed(true); //trap the mouse (NOT WORKING AS INTENDED)
            appgc.start();
        }catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public static void setFullscreen(AppGameContainer appgc) throws SlickException{
        appgc.setDisplayMode(appgc.getScreenWidth(), appgc.getScreenHeight(), true);
        SCALE = appgc.getScreenWidth() / WIDTH;
        SCALED_WIDTH = appgc.getScreenWidth();
        SCALED_HEIGHT = appgc.getScreenHeight();
    }

    public static int randomWithRange(int min, int max)
    {
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }
}